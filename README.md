# Docker Jekyll Base
Quickly create a local docker environment that runs [Jekyll](https://jekyllrb.com/docs/home/), a simple, blog-aware, static site generator

## Requirements
- [Docker](https://www.docker.com/)

## Quick Start
```
$> docker-compose up
```

Once the jekyll container is built, navigate in your browser to `http://localhost:4000`