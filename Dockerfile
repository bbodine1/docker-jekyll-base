FROM jekyll/jekyll

RUN gem install bundler

USER jekyll

ADD ./Gemfile /srv/jekyll

RUN bundle

ADD . /srv/jekyll

RUN jekyll build
